# humanstxt.org/
# The humans responsible & technology colophon

# THANKS

    < Thom Maurick >

# TECHNOLOGY COLOPHON

    Mobile First, Responsive Design, Progressive Web Apps
    CSS3, HTML5, JavaScript, ES6
    Node.js, NPM, NPM Scripts,
    Sass, Pug, Babel,
    Browserify, Autoprefixer, UnCSS
    BrowserSync, ImageMin
    Visual Studio Code, Powershell, Chrome
